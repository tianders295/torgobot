import xml.etree.ElementTree as ETree
import os


class Metadata:
    def __init__(self, file):
        self.file = file
        self.root = ETree.parse(self.file).getroot()
        self.prefix = ""
        self.main_token = ""
        self.test_token = ""
        self.description = ""
        self.working_dir = ""
        self.master = ""
        self.test_mode = False
        # self.commands = CommandsMetadata()

    """
    def get_commands(self):
        commands = self.root.find('commands')
        for command in commands.findall('command'):
            name = command.find('name').text
            brief = command.find('brief').text
            description = command.find('description').text
            usage = command.find('usage').text
            setattr(self.commands, name, CommandMetadata(brief, description, usage))
    """

    def get_bot(self):
        self.main_token = self.root.find('main_token').text
        self.test_token = self.root.find('test_token').text
        self.prefix = self.root.find('prefix').text
        self.working_dir = self.root.find('working_dir').text
        if self.working_dir == 'default':
            self.working_dir = os.getcwd()
        self.test_mode = self.root.find('test_mode').text.lower() == "true"
        self.master = self.root.find('master').text
        self.description = self.root.find('description').text.format(self.master)

"""
class CommandsMetadata:
    pass


class CommandMetadata:
    def __init__(self, brief, description, usage):
        self.brief = brief
        self.description = description
        self.usage = usage
"""

