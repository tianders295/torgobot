import discord
from discord.ext import commands
from datetime import datetime
from torgoutils import ANSI


class Events(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.in_call_role = "In Call"
        self.ansi = ANSI()

    @commands.Cog.listener()
    async def on_ready(self):
        now = datetime.today().strftime("%B %d %Y, at %I:%M:%S %p")
        print(f"{now}:\n{self.ansi.color['green']}Torgobot is ready!{self.ansi.clear}")
        print("="*64)
        print("="*64)
        await self.bot.change_presence(
            status=discord.Status.online,
            activity=discord.Game(name='$help')
        )

    @commands.Cog.listener()
    async def on_command(self, ctx):
        now = datetime.today().strftime("%B %d %Y, at %I:%M:%S %p")
        print("_" * 64)
        print(f"{now}:\nCommand ${ctx.invoked_with} CALLED. Info:")
        print(f"*  Author:    {ctx.author.display_name} ({ctx.author.name}#{ctx.author.discriminator})")
        print(f"*  Channel:   #{ctx.channel.name}")
        print(f"*  Guild:     {ctx.guild.name} ({ctx.guild.id})")
        print(f"*  Content:   {ctx.message.content}")
        print("_" * 64)

    @commands.Cog.listener()
    async def on_command_completion(self, ctx):
        now = datetime.today().strftime("%B %d %Y, at %I:%M:%S %p")
        print("_" * 64)
        print(f"{now}:\nCommand ${ctx.invoked_with} {self.ansi.color['green']}COMPLETED.{self.ansi.clear} Info:")
        print(f"*  Author:    {ctx.author.display_name} ({ctx.author.name}#{ctx.author.discriminator})")
        print(f"*  Channel:   #{ctx.channel.name}")
        print(f"*  Guild:     {ctx.guild.name} ({ctx.guild.id})")
        print(f"*  Content:   {ctx.message.content}")
        print("_" * 64)

    @commands.Cog.listener()
    async def on_command_error(self, ctx, error):
        if isinstance(error, commands.CommandNotFound):
            await ctx.send(f"ERROR: `${ctx.invoked_with}` is not a command I recognize. "
                           f"Use `$help` to show a list of commands.")
        now = datetime.today().strftime("%B %d %Y, at %I:%M:%S %p")
        print("_" * 64)
        print(f"{now}:\nCommand ${ctx.invoked_with} {self.ansi.color['red']}FAILED.{self.ansi.clear} Info:")
        print(f"*  Author:    {ctx.author.display_name} ({ctx.author.name}#{ctx.author.discriminator})")
        print(f"*  Channel:   #{ctx.channel.name}")
        print(f"*  Guild:     {ctx.guild.name} ({ctx.guild.id})")
        print(f"*  Content:   {ctx.message.content}")
        print(f"*  Error:     {repr(error)}")
        print("_" * 64)



    @commands.Cog.listener()
    async def on_voice_state_update(self, member, before, after):
        now = datetime.today().strftime("%B %d %Y, at %I:%M:%S %p")
        channel_name = ""
        mode = ""
        status = f"{self.ansi.color['green']}OK{self.ansi.clear}"
        errors = "None."
        if discord.utils.get(member.guild.roles, name=self.in_call_role) is None:
            await member.guild.create_role(name=self.in_call_role)
        if before.channel is None:
            mode = f"{self.ansi.color['yellow']}add{self.ansi.clear}"
            channel_name = after.channel.name
            try:
                role = discord.utils.get(member.guild.roles, name=self.in_call_role)
                await member.add_roles(role)
            except Exception as e:
                errors = repr(e)
                status = f"{self.ansi.color['red']}ERROR{self.ansi.clear}"
        elif after.channel is None:
            channel_name = before.channel.name
            mode = f"{self.ansi.color['yellow']}revoke{self.ansi.clear}"
            try:
                role = discord.utils.get(member.guild.roles, name=self.in_call_role)
                await member.remove_roles(role)
            except Exception as e:
                errors = repr(e)
                status = f"{self.ansi.color['red']}ERROR{self.ansi.clear}"
        print("_" * 64)
        print(f"{now}:\n{status}: Attempted to {mode} voice role. Info:")
        print(f"*  Member:    {member.display_name} ({member.name}#{member.discriminator})")
        print(f"*  Channel:   #{channel_name}")
        print(f"*  Guild:     {member.guild.name} ({member.guild.id})")
        print(f"*  Errors:    {errors}")
        print("_" * 64)


def setup(bot):
    bot.add_cog(Events(bot))