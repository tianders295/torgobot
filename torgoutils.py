import requests
import random
import re


def open_letters_file(file_):
    dictionary = {}
    with open(file_, 'r', encoding='utf-8') as f:
        lines = f.readlines()
    line_stripped = lines[0].strip()
    for i, char in enumerate(line_stripped):
        dictionary[char] = lines[1][i]
        print(lines[1][i])
    return dictionary


def make_temp_file_name(file_type, length=8):
    random_name = ""
    # create a random image name
    for i in range(length):
        random_name += chr(random.randint(ord('a'), ord('z')))
    temp_file = 'temp/' + random_name + file_type
    return temp_file


def save_image(pic_url, file_type):
    temp_file = make_temp_file_name(file_type, length=8)
    # save the image
    with open(temp_file, 'wb') as handle:
        response = requests.get(pic_url, stream=True)
        if not response.ok:
            print(response)
        for block in response.iter_content(1024):
            if not block:
                break
            handle.write(block)
        return temp_file


def convert_to_color_int(hex_string):
    if not hex_string.startswith('#') or len(hex_string) != 7:
        return None
    h = hex_string.replace('#', '0x')
    return int(h, 0)


def scan_message(text, word):
    if re.search(word, text, re.IGNORECASE):
        return True
    return False


def check_profanity(text, word_list):
    for word in word_list:
        if scan_message(text, word):
            return True
    return False


class ANSI:
    def __init__(self):
        self.color = self.build_colors()
        self.background = self.build_background()
        self.clear = "\u001b[0m"
        self.bold = "\u001b[1m"
        self.underline = "\u001b[4m"

    def build_colors(self):
        format_dict = {
            # basic colors
            "black": "\u001b[30m",
            "red": "\u001b[31m",
            "green": "\u001b[32m",
            "yellow": "\u001b[33m",
            "blue": "\u001b[34m",
            "magenta": "\u001b[35m",
            "cyan": "\u001b[36m",
            "gray": "\u001b[37m",
            # bright colors
            "charcoal": "\u001b[30;1m",
            "pink": "\u001b[31;1m",
            "chartreuse": "\u001b[32;1m",
            "cream": "\u001b[33;1m",
            "skyblue": "\u001b[34;1m",
            "fuchsia": "\u001b[35;1m",
            "teal": "\u001b[36;1m",
            "white": "\u001b[37;1m",
            # bg colors
        }
        return format_dict

    def build_background(self):
        format_dict = {
            "black": "\u001b[40m",
            "red": "\u001b[41m",
            "green": "\u001b[42m",
            "yellow": "\u001b[43m",
            "blue": "\u001b[44m",
            "magenta": "\u001b[45m",
            "teal": "\u001b[46m",
            "gray": "\u001b{47m",
            # bright bg colors
            "charcoal": "\u001b[40;1m",
            "pink": "\u001b[41;1m",
            "chartreuse": "\u001b[42;1m",
            "cream": "\u001b[43;1m",
            "skyblue": "\u001b[44;1m",
            "fuchsia": "\u001b[45;1m",
            "cyan": "\u001b[46;1m",
            "white": "\u001b{47;1m",
        }
        return format_dict

